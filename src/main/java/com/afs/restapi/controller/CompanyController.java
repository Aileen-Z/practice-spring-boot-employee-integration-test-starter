package com.afs.restapi.controller;


import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/companies")
@RestController
public class CompanyController {
    @Autowired
    CompanyService companyService;
    @GetMapping()
    public List<Company> listCompanies() {
        return companyService.listCompanies();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Company create(@RequestBody Company company) {
        return companyService.addCompany(company);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> listCompanyEmployees(@PathVariable Long companyId) {
        return companyService.listCompanyEmployees(companyId);
    }
    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return companyService.getCompaniesByPage(page, size);

    }
    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id, @RequestBody Company company) {
        return companyService.updateCompanyName(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Boolean deleteCompany(@PathVariable Long id) {
        return companyService.deleteCompany(id);
    }
}
