package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> listCompanies() {
        return companyRepository.findAll();
    }
    public Company addCompany(Company company) {
        company.setName(company.getName());
        return companyRepository.insert(company);
    }
    public List<Employee> listCompanyEmployees(Long companyId) {
        return employeeRepository.findByCompanyId(companyId);
    }
    public List<Company> getCompaniesByPage(Integer page, Integer size) {
        return companyRepository.findByPage(page, size);
    }
    public Company updateCompanyName(Long id, Company company) {
        Company companyToUpdate = companyRepository.findById(id);
        companyToUpdate.setName(company.getName());
        return companyRepository.update(companyToUpdate);
    }
    public Boolean deleteCompany(Long id) {
        return null;
    }
}
