package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.AgeIsInvaildException;
import com.afs.restapi.exception.CanNotUpdateException;
import com.afs.restapi.exception.IsNotTalentedException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    EmployeeRepository employeeRepository;
    public EmployeeService(EmployeeRepository employeeRepository){this.employeeRepository = employeeRepository;}
    public List<Employee> findAll(){return employeeRepository.findAll();}


    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public Employee create(Employee employee) {
        if (!employee.isAgeValid()) {
            throw new AgeIsInvaildException();
        }
        if (employee.isNotTalented()) {
            throw new IsNotTalentedException();
        }
        employee.setActive(true);
        return employeeRepository.insert(employee);
    }

    public Employee insert(Employee employee) {
        return employeeRepository.insert(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = employeeRepository.findById(id);
        if(employeeToUpdate.getActive() == false){
            throw new CanNotUpdateException();
        }
        employeeToUpdate.setAge(employee.getAge());
        employeeToUpdate.setSalary(employee.getSalary());
        return employeeRepository.update(employeeToUpdate);
    }

    public void delete(Long id) {
        Employee toRemovedEmployee = employeeRepository.findById(id);
        if(toRemovedEmployee != null){
            toRemovedEmployee.setActive(false);
            employeeRepository.update(toRemovedEmployee);
        }
        else{
            throw new NotFoundException();
        }
    }
}
