package com.afs.restapi;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc client;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void cleanEmployeeData() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employee_when_given_a_employee() throws Exception {
        Employee employee = new Employee(null, "John", 22, "Male", 5000);
        employeeRepository.insert(employee);
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(employee.getId()))
                .andExpect(jsonPath("$[0].name").value(employee.getName()))
                .andExpect(jsonPath("$[0].salary").value(employee.getSalary()))
                .andExpect(jsonPath("$[0].age").value(employee.getAge()))
        ;
    }
    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
        Employee employee = new Employee(null, "John", 22, "Male", 5000);
        employeeRepository.insert(employee);
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath(("$[0].id")).value(employee.getId()));
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        Employee employee = new Employee(null, "John", 22, "Male", 5000);
        String joinJson = new ObjectMapper().writeValueAsString(employee);
        client.perform(MockMvcRequestBuilders.post("/employees")
                .content(joinJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(employee.getName()));
    }
    @Test
    void should_return_employee_when_perform_getEmployeeById_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John", 22, "Male", 5000);
        employeeRepository.insert(john);
        client.perform(MockMvcRequestBuilders.get("/employees/" + john.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(john.getId()))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
        ;
    }

    @Test
    void should_return_employees_when_perform_findByPage_given_page_and_size() throws Exception {
        Integer page = 1;
        Integer size = 5;
        Employee lily1 = new Employee(null, "Lily1", 20, "Female", 8000);
        Employee lily2 = new Employee(null, "Lily2", 20, "Female", 8000);
        Employee lily3 = new Employee(null, "Lily3", 20, "Female", 8000);
        Employee lily4 = new Employee(null, "Lily4", 20, "Female", 8000);
        Employee lily5 = new Employee(null, "Lily5", 20, "Female", 8000);
        Employee lily6 = new Employee(null, "Lily6", 20, "Female", 8000);
        employeeRepository.insert(lily1);
        employeeRepository.insert(lily2);
        employeeRepository.insert(lily3);
        employeeRepository.insert(lily4);
        employeeRepository.insert(lily5);
        employeeRepository.insert(lily6);
        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value(lily1.getName()));
        ;
    }
    @Test
    void should_return_employee_when_perform_updateEmployeeById_given_employee_id() throws Exception {
        // Given
        Employee jo = new Employee(1L, "Jo", 22, "Male", 5000,true);
        employeeRepository.insert(jo);
        Employee john = new Employee(1L, "John Smith", 33, "Male", 500,true);
        String johnJson = new ObjectMapper().writeValueAsString(john);
        // When
        client.perform(MockMvcRequestBuilders
                .put("/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(johnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(jo.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value(jo.getGender()))
                .andExpect(jsonPath("$.salary").value(john.getSalary()));
    }

    @Test
    void should_return_204_status_code_when_perform_deleteEmployee_given_employeeId() throws Exception {
        Employee lily1 = new Employee(null, "Lily1", 20, "Female", 8000);
        Employee lily2 = new Employee(null, "Lily2", 20, "Female", 8000);
        employeeRepository.insert(lily1);
        employeeRepository.insert(lily2);
        client.perform(MockMvcRequestBuilders.delete("/employees/" + lily1.getId()))
                .andExpect(status().isNoContent());
    }
}
