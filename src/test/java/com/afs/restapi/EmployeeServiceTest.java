package com.afs.restapi;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.AgeIsInvaildException;
import com.afs.restapi.exception.CanNotUpdateException;
import com.afs.restapi.exception.IsNotTalentedException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EmployeeServiceTest {
    EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee employee = new Employee(null, "dz", 15, "Male", 6000);
        assertThrows(AgeIsInvaildException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_create_successfully_when_create_employee_given_age_is_valid() {
        EmployeeRepository employeeRepository = this.employeeRepository;
        Employee employee = new Employee(1L, "dz", 20, "Male", 6000);
        given(employeeRepository.insert(any())).willReturn(employee);

        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee createdEmployee = employeeService.create(employee);

        assertThat(createdEmployee.getId()).isEqualTo(1);
        assertThat(createdEmployee.getName()).isEqualTo("dz");
        assertThat(createdEmployee.getSalary()).isEqualTo(6000);
        assertThat(createdEmployee.getGender()).isEqualTo("Male");

        verify(employeeRepository).insert(argThat(employee1 -> {
            assertThat(createdEmployee.getId()).isEqualTo(1);
            assertThat(createdEmployee.getName()).isEqualTo("dz");
            assertThat(createdEmployee.getSalary()).isEqualTo(6000);
            assertThat(createdEmployee.getGender()).isEqualTo("Male");
            return true;
        }));
    }

    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_over_30_and_salary_below_20000() {
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee employee = new Employee(null, "dz", 40, "Male", 6000);
        assertThrows(IsNotTalentedException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_create_successfully_and_active_true_when_create_employee() {
        EmployeeRepository employeeRepository = this.employeeRepository;
        Employee employee = new Employee(1L, "dz", 20, "Male", 6000);
        given(employeeRepository.insert(any())).willReturn(employee);

        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee createdEmployee = employeeService.create(employee);

        assertThat(createdEmployee.getId()).isEqualTo(1);
        assertThat(createdEmployee.getName()).isEqualTo("dz");
        assertThat(createdEmployee.getSalary()).isEqualTo(6000);
        assertThat(createdEmployee.getGender()).isEqualTo("Male");
        assertThat(createdEmployee.getActive()).isEqualTo(true);

        verify(employeeRepository).insert(argThat(employee1 -> {
            assertThat(createdEmployee.getId()).isEqualTo(1);
            assertThat(createdEmployee.getName()).isEqualTo("dz");
            assertThat(createdEmployee.getSalary()).isEqualTo(6000);
            assertThat(createdEmployee.getGender()).isEqualTo("Male");
            assertThat(createdEmployee.getActive()).isEqualTo(true);
            return true;
        }));
    }

    @Test
    void should_update_when_update_employee_given_active_is_true() {
        EmployeeRepository employeeRepository = this.employeeRepository;
        Employee employee = new Employee(null, "jo", 20, "Male", 6000,true);
        given(employeeRepository.findById(employee.getId())).willReturn(employee);
        Employee updateEmployee = new Employee(employee.getId(), "john", 20, "Male", 8000,true);
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        employeeService.update(employee.getId(), updateEmployee);
        assertThat(updateEmployee.getName()).isEqualTo("john");
        assertThat(updateEmployee.getSalary()).isEqualTo(8000);
        assertThat(updateEmployee.getGender()).isEqualTo("Male");
        assertThat(updateEmployee.getActive()).isEqualTo(true);

        verify(employeeRepository).findById(argThat(id -> {
            assertThat(id).isEqualTo(employee.getId());
            return true;
        }));
    }
    @Test
    void should_not_update_when_update_employee_given_active_is_false() {
        EmployeeRepository employeeRepository = this.employeeRepository;
        Employee employee = new Employee(null, "jo", 20, "Male", 6000,false);
        given(employeeRepository.findById(employee.getId())).willReturn(employee);
        Employee updateEmployee = new Employee(employee.getId(), "john", 20, "Male", 8000);
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        assertThrows(CanNotUpdateException.class, () -> employeeService.update(updateEmployee.getId(),updateEmployee));
    }
}
