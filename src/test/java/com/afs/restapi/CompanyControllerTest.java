package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc client;

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void cleanData() {
        companyRepository.clearAll();
        employeeRepository.clearAll();
    }

    @Test
    void should_return_companies_when_given_companies() throws Exception {
        Company oocl = new Company(null, "OOCL");
        Company tw = new Company(null, "TW");
        companyRepository.insert(oocl);
        companyRepository.insert(tw);
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(oocl.getId()))
                .andExpect(jsonPath("$[0].name").value(oocl.getName()))
        ;
    }

    @Test
    void should_a_company_when_given_a_company() throws Exception {
        Company oocl = new Company(null, "OOCL");
        companyRepository.insert(oocl);
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(oocl.getId()))
                .andExpect(jsonPath("$[0].name").value(oocl.getName()))
        ;
    }

    @Test
    void should_return_created_company_when_perform_create_given_company_json() throws Exception {
        Company oocl = new Company(null, "OOCL");
        String joinJson = new ObjectMapper().writeValueAsString(oocl);
        client.perform(MockMvcRequestBuilders.post("/companies")
                .content(joinJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(oocl.getName()))
        ;
    }
    @Test
    void should_return_employees_when_perform_listCompanyEmployees_given_company_id() throws Exception {
        Employee john1 = new Employee(null, "John1", 22, "Male", 5000, 1L);
        Employee john2 = new Employee(null, "John2", 23, "Female", 5000, 2L);
        Employee john3 = new Employee(null, "John3", 24, "Male", 5000, 1L);
        employeeRepository.insert(john1);
        employeeRepository.insert(john2);
        employeeRepository.insert(john3);
        Company oocl = new Company(null, "OOCL");
        Company tw = new Company(null, "TW");
        companyRepository.insert(oocl);
        companyRepository.insert(tw);
        client.perform(MockMvcRequestBuilders.get("/companies/" + oocl.getId() + "/employees/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(john1.getId()))
                .andExpect(jsonPath("$[0].name").value(john1.getName()))
                .andExpect(jsonPath("$[0].age").value(john1.getAge()))
        ;
    }
    @Test
    void should_return_companies_when_perform_getCompaniesByPage_given_page_and_size() throws Exception {
        Company oocl = new Company(null, "OOCL");
        Company tw = new Company(null, "TW");
        Company cosco = new Company(null, "COSCO");
        companyRepository.insert(oocl);
        companyRepository.insert(tw);
        companyRepository.insert(cosco);
        client.perform(MockMvcRequestBuilders.get("/companies").param("page", "1").param("size", "2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name").value(oocl.getName()));
        ;
    }
    @Test
    void should_return_company_when_perform_updateCompanyName_given_company_id() throws Exception {
        // Given
        Company oocl = new Company(null, "OOCL");
        companyRepository.insert(oocl);
        Company cosco = new Company(oocl.getId(), "COSCO");
        String johnJson = new ObjectMapper().writeValueAsString(cosco);
        // When
        client.perform(MockMvcRequestBuilders
                .put("/companies/" + oocl.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(johnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(cosco.getName()));
    }
    @Test
    void should_return_204_status_code_when_perform_deleteCompany_given_companyId() throws Exception {
        Company oocl = new Company(null, "OOCL");
        Company tw = new Company(null, "TW");
        companyRepository.insert(oocl);
        companyRepository.insert(tw);
        client.perform(MockMvcRequestBuilders.delete("/companies/" + oocl.getId()))
                .andExpect(status().isNoContent());
    }
}
